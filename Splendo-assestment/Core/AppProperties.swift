//
//  AppProperties.swift
//  Splendo-assestment
//
//  Created by tair on 1.05.22.
//

import Foundation
import Foundation

struct AppProperties {
    @UserDefault("Liked", defaultValue: [])
    static var liked: [String]
}
