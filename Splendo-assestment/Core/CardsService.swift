//
//  CardsService.swift
//  Splendo-assestment
//
//  Created by tair on 1.05.22.
//

import Foundation

protocol CardsLoadingService {
    associatedtype LoadableData
    func loadData() -> LoadableData?
}

class CardsService: CardsLoadingService {

    typealias LoadableData = CardData

    func loadData() -> LoadableData? {
        do {
            if let url = Bundle.main.url(forResource: CardsListViewModel.fileName, withExtension: "json") {
                let data = try? Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(CardData.self, from: data!)
                return jsonData
            } else {
                return nil
            }
        } catch let DecodingError.dataCorrupted(context) {
            print(context)
        } catch let DecodingError.keyNotFound(key, context) {
            print("Key '\(key)' not found:", context.debugDescription)
            print("codingPath:", context.codingPath)
        } catch let DecodingError.valueNotFound(value, context) {
            print("Value '\(value)' not found:", context.debugDescription)
            print("codingPath:", context.codingPath)
        } catch let DecodingError.typeMismatch(type, context)  {
            print("Type '\(type)' mismatch:", context.debugDescription)
            print("codingPath:", context.codingPath)
        } catch {
            print("error: ", error)
        }
        return nil
    }
}
