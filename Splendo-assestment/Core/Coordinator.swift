//
//  Factory.swift
//  Splendo-assestment
//
//  Created by tair on 15.04.22.
//

import Foundation
import UIKit
import SwiftUI

protocol CoordinatorProtocol {
    associatedtype StartView
    func start() -> StartView?
}

final class Coordinator: CoordinatorProtocol {
    
    let childCoordinator = MainFlowCoordinator()
    typealias StartView = MainFlowCoordinator.StartView
    
    func start() -> StartView? {
        childCoordinator.start()
    }
    
}
