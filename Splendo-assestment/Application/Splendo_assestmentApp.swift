//
//  Splendo_assestmentApp.swift
//  Splendo-assestment
//
//  Created by tair on 15.04.22.
//

import SwiftUI

@main
struct Splendo_assestmentApp: App {
    
    let coordinator = Coordinator()
    
    var body: some Scene {
        WindowGroup {
            NavigationView {
                coordinator.start()
            }
            .navigationViewStyle(.stack)

        }
    }
}
