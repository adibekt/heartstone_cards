//
//  Colors.swift
//  Splendo-assestment
//
//  Created by tair on 30.04.22.
//

import Foundation
import SwiftUI

extension Color {
    static let background2 = Color(uiColor: UIColor(named: "Background2")!)
}
