//
//  TypeAliases.swift
//  Splendo-assestment
//
//  Created by tair on 30.04.22.
//

import Foundation
import SwiftUI

typealias FilterClosure = (Filter) -> ()
typealias CardClosure = (Basic) -> ()
typealias CardsRowData = (`left`: Basic, `right`: Basic?)
typealias VoidClosure = () -> (Void)
typealias ImageClosure = (UIImage) -> ()
