//
//  LargeImageView.swift
//  Splendo-assestment
//
//  Created by tair on 1.05.22.
//

import SwiftUI

struct LargeImageView: View {
    var image: UIImage
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        GeometryReader { proxy in
            ZStack {
                Color(UIColor.black).ignoresSafeArea()
                VStack {
                    Button {
                        dismiss()
                    } label: {
                        Image(systemName: "xmark")
                            .foregroundColor(.white)
                            .frame(width: 35, height: 35, alignment: .center)
                            .padding(15)
                    }.frame(maxWidth: .infinity, alignment: .trailing)
                    
                    Image(uiImage: image)
                        .frame(width: proxy.size.width, height: proxy.size.height * 0.6, alignment: .center)
                    Spacer()
                }
            }
        }
    }
}

//struct LargeImageView_Previews: PreviewProvider {
//    static var previews: some View {
//        LargeImageView()
//    }
//}
