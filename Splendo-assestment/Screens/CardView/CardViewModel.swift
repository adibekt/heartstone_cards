//
//  CardViewModel.swift
//  Splendo-assestment
//
//  Created by tair on 1.05.22.
//

import Foundation
import SwiftUI

class CardViewModel: ObservableObject {
    var item: Basic
    @Published var isLiked: Bool
    @Published var imagePresented = false
    weak var coordinator: MainFlowCoordinatorProtocol?
    var image: UIImage?
    
    init(card: Basic, coordinator: MainFlowCoordinatorProtocol?) {
        item = card
        self.coordinator = coordinator
        isLiked = AppProperties.liked.contains(item.id)
    }
    
    func onLikePressed() {
        isLiked = !isLiked
        if AppProperties.liked.contains(item.id) {
            AppProperties.liked.removeAll(where: { $0 == item.id })
        } else {
            AppProperties.liked.append(item.id)
        }
    }
    
    func set(image: UIImage) {
        self.image = image
        imagePresented = true
    }
    
    func showImage() -> LargeImageView? {
        guard let image = image else {
            return nil
        }
        return coordinator?.getImageView(with: image)
    }
    
}
