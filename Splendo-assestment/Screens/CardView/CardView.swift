//
//  CardView.swift
//  Splendo-assestment
//
//  Created by tair on 1.05.22.
//

import SwiftUI

struct CardView: View {
    
    @ObservedObject var viewModel: CardViewModel
    @Environment(\.presentationMode) var presentationMode

    var body: some View {
        ZStack {
            Color.background2.ignoresSafeArea()
            ScrollView {
                VStack {
                    HStack {
                        Image(systemName: viewModel.isLiked ? "suit.heart.fill" : "heart")
                            .foregroundColor(.red)
                            .frame(width: 24, height: 24, alignment: .center)
                            .padding(15)
                            .onTapGesture {
                                viewModel.onLikePressed()
                            }
                        Spacer()
                        Button {
                            presentationMode.wrappedValue.dismiss()
                        } label: {
                            Image(systemName: "xmark")
                                .foregroundColor(.white)
                                .frame(width: 35, height: 35, alignment: .center)
                                .padding(15)
                            
                        }
                    }
                    
                    Text(viewModel.item.name)
                        .foregroundColor(.white)
                        .font(.system(.title))
                        .bold()
                        .padding(.top, 10)
                    
                    if let img = viewModel.item.img {
                        ImageView(withURL: img) { image in
                            viewModel.set(image: image)
                        }
                    }
                    
                    ParametersView(data: ("Card Set", viewModel.item.cardSet.rawValue))
                    ParametersView(data: ("Type", viewModel.item.type.rawValue))
                    ParametersView(data: ("Rarity", viewModel.item.rarity?.rawValue))
                    ParametersView(data: ("Race", viewModel.item.race?.rawValue))
                    ParametersView(data: ("Faction", viewModel.item.faction?.rawValue))
                    ParametersView(data: ("Player class", viewModel.item.playerClass?.rawValue))
                    ParametersView(data: ("Flavor", viewModel.item.flavor))
                }.sheet(isPresented: $viewModel.imagePresented) {
                    viewModel.showImage()
                }
            }
        }
    }
}

struct ParametersView: View {
    
    var data: (key: String, value: String?)
    
    var body: some View {
        HStack {
            if let value = data.value {
                Text(data.key + ":")
                    .font(.system(.title2))
                    .italic()
                    .foregroundColor(.white)
                Spacer()
                Text(value)
                    .font(.system(.title2))
                    .bold()
                    .foregroundColor(.white)
            }
        }.padding(.horizontal, 12)
    }
}

//struct CardView_Previews: PreviewProvider {
//    static var previews: some View {
//        CardView(viewModel: CardViewModel())
//    }
//}
