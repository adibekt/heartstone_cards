//
//  FilterView.swift
//  Splendo-assestment
//
//  Created by tair on 30.04.22.
//

import SwiftUI

struct FilterView: View {
    @State var filter: Filter
    var onDismiss: FilterClosure
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        ZStack {
            Color.background2.ignoresSafeArea()
            VStack {
                Button {
                    onDismiss(filter)
                    dismiss()
                } label: {
                    Image(systemName: "xmark")
                        .foregroundColor(.white)
                        .frame(width: 35, height: 35, alignment: .center)
                        .padding(15)
                    
                }.frame(maxWidth: .infinity, alignment: .trailing)

                ScrollView {
                    VStack(alignment: .leading, spacing: 10, content: {
                        Text("Rarity").font(.system(.title2)).bold()
                        ForEach(Rarity.allCases, id: \.self) { item in
                            HStack {
                                ToggleView(isSelected: filter.rarity.contains(item),
                                           title: item.rawValue, item: item) { (item: Rarity) in
                                    if filter.rarity.contains(item) {
                                        filter.rarity.removeAll(where: { $0 == item })
                                    } else {
                                        filter.rarity.append(item)
                                    }
                                }
                            }
                        }
                    }).padding()
                    
                    VStack(alignment: .leading, spacing: 10, content: {
                        Text("Classes").font(.system(.title2)).bold()
                        ForEach(Class.allCases, id: \.self) { item in
                            HStack {
                                ToggleView(isSelected: filter.classes.contains(item),
                                           title: item.rawValue, item: item) { (item: Class) in
                                    if filter.classes.contains(item) {
                                        filter.classes.removeAll(where: { $0 == item })
                                    } else {
                                        filter.classes.append(item)
                                    }
                                }
                            }
                        }
                    }).padding()
                    
                    VStack(alignment: .leading, spacing: 10, content: {
                        Text("Mechanics").font(.system(.title2)).bold()
                        ForEach(Name.allCases, id: \.self) { item in
                            HStack {
                                ToggleView(isSelected: filter.mechanics.contains(item),
                                           title: item.rawValue, item: item) { (item: Name) in
                                    if filter.mechanics.contains(item) {
                                        filter.mechanics.removeAll(where: { $0 == item })
                                    } else {
                                        filter.mechanics.append(item)
                                    }
                                }
                            }
                        }
                    }).padding()
                    
                    VStack(alignment: .leading, spacing: 10, content: {
                        Text("Type").font(.system(.title2)).bold()
                        ForEach(TypeEnum.allCases, id: \.self) { item in
                            HStack {
                                ToggleView(isSelected: filter.type.contains(item),
                                           title: item.rawValue, item: item) { (item: TypeEnum) in
                                    if filter.type.contains(item) {
                                        filter.type.removeAll(where: { $0 == item })
                                    } else {
                                        filter.type.append(item)
                                    }
                                }
                            }
                        }
                    }).padding()
                }.frame(maxWidth: .infinity, alignment: .center)
            }
        }
        
    }
}
struct ToggleView<Item>: View {
    @State var isSelected: Bool
    var title: String
    var item: Item
    var onTap: (Item) -> (Void)
    var body: some View {
        HStack {
            Text(title)
            Spacer()
            Image(systemName: "checkmark").opacity(isSelected ? 1 : 0)
        }.foregroundColor(isSelected ? .cyan : .secondary)
            .onTapGesture {
                self.isSelected = !isSelected
                self.onTap(item)
            }
    }
}
