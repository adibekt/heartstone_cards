//
//  MainFlowCoordinator.swift
//  Splendo-assestment
//
//  Created by Tair Adibek on 28/07/2022.
//

import Foundation
import UIKit
import SwiftUI

protocol MainFlowCoordinatorProtocol: AnyObject {
    func getFiltersView(with filter: Filter, completion: @escaping FilterClosure) -> FilterView
    func getCardView(with card: Basic) -> CardView
    func getImageView(with image: UIImage) -> LargeImageView
}

final class MainFlowCoordinator: CoordinatorProtocol, MainFlowCoordinatorProtocol {
    
    typealias StartView = CardsListView
    lazy var cardsService = CardsService()

    func start() -> StartView? {
        guard let data = cardsService.loadData() else {
            return nil
        }
        let viewModel = CardsListViewModel(cardData: data, coordinator: self)
        return CardsListView(viewModel: viewModel)
    }
    
    func getFiltersView(with filter: Filter, completion: @escaping FilterClosure) -> FilterView {
        return FilterView(filter: filter, onDismiss: completion)
    }
    
    func getCardView(with card: Basic) -> CardView {
        let viewModel = CardViewModel(card: card, coordinator: self)
        return CardView(viewModel: viewModel)
    }
    
    func getImageView(with image: UIImage) -> LargeImageView {
        return LargeImageView(image: image)
    }
}
