//
//  CardsListView.swift
//  Splendo-assestment
//
//  Created by tair on 17.04.22.
//

import SwiftUI

struct CardsListView: View {
    
    @ObservedObject var viewModel: CardsListViewModel
    let itemPerRow: CGFloat = 2
    let horizontalSpacing: CGFloat = 8
    @State var isFilterPresented = false
    @State var isCardPresented = false
    
    init(viewModel: CardsListViewModel) {
        self.viewModel = viewModel
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor.white, .font: UIFont.systemFont(ofSize: 24, weight: .heavy)]
    }
    
    var body: some View {
        ZStack {
            Color(UIColor.black).ignoresSafeArea()
            VStack {
                ScrollView(.horizontal, showsIndicators: false) {
                    HStack(alignment: .center, spacing: 5) {
                        ForEach(0..<$viewModel.fields.count) { i in
                            let item = viewModel.fields[i]
                            Text(viewModel.fields[i].stringValue)
                                .foregroundColor(($viewModel.currentCategory.wrappedValue == item) ? .cyan : .secondary)
                                .font(.title2)
                                .padding(.horizontal, 5)
                                .onTapGesture {
                                    viewModel.set(item: item)
                                }
                        }
                    }.padding(10)
                }
                
                ScrollView {
                    VStack(alignment: .leading, spacing: 8) {
                        ForEach($viewModel.cards.indices) { i in
                            if i % Int(itemPerRow) == 0 {
                                buildView(rowIndex: i, onTap: { item in
                                    self.viewModel.setSelected(card: item)
                                    self.isCardPresented = true
                                })
                                
                            }
                        }
                    }
                }
            }
            .sheet(isPresented: $isFilterPresented, content: {
                self.viewModel.showFilter()
            })
            .fullScreenCover(isPresented: $isCardPresented, content: {
                self.viewModel.showCardView()
            })
        }
        .navigationBarTitleDisplayMode(.inline)
        .navigationTitle("HeartStone Cards")
        .toolbar {
            Button {
                isFilterPresented = true
            } label: {
                Image(systemName: "line.3.horizontal.decrease.circle")
                    .foregroundColor(.white)
            }.accessibilityLabel("filter")
        }
    }
    
    func buildView(rowIndex: Int, onTap: @escaping CardClosure) -> RowView? {
        let data: CardsRowData
        if (viewModel.cards.count) > rowIndex + 1 {
            data = (viewModel.cards[rowIndex], viewModel.cards[rowIndex + 1])
        } else if (viewModel.cards.count) == rowIndex + 1 {
            data = (viewModel.cards[rowIndex], nil)
        } else {
            return nil
        }
        return RowView(cardsRowData: data, horizontalSpacing: horizontalSpacing,
                       onTap: onTap)
    }
    
}
//struct CardsListView_Previews: PreviewProvider {
//    static var previews: some View {
//        CardsListView(viewModel: CardsListViewModel(coordinator: Coordinator()))
//    }
//}
