//
//  CardCellView.swift
//  Splendo-assestment
//
//  Created by tair on 17.04.22.
//

import SwiftUI
import Combine

struct CardCellView: View {
    var data: Basic
    var onTap: CardClosure
    var body: some View {
        VStack {
            if let img = data.img {
                ImageView(withURL: img) { _ in
                    onTap(data)
                }
            } else {
                Image("female-placeholder")
                    .resizable()
                    .cornerRadius(15)
                    .aspectRatio(contentMode: .fit)
                    .frame(maxWidth: getWidth(), maxHeight: 190)
                    .padding(.top, 45)
            }
            Spacer()
            Text(data.name)
                .foregroundColor(.white)
                .font(.title3)
                .padding()
        }.onTapGesture {
            onTap(data)
        }
    }
    
    func getWidth() -> CGFloat {
        let width: CGFloat = (UIScreen.main.bounds.size.width - 6) / 2
        return width
    }
}

struct RowView: View {
    let cardsRowData: CardsRowData
    let horizontalSpacing: CGFloat
    var onTap: CardClosure
    var body: some View {
        HStack(spacing: horizontalSpacing) {
            CardCellView(data: cardsRowData.left) { item in
                onTap(item)
            }
            if let rightDatum = cardsRowData.right {
                CardCellView(data: rightDatum) { item in
                    onTap(item)
                }
            }
        }
        .padding()
    }
}

class ImageLoader: ObservableObject {
    var didChange = PassthroughSubject<Data, Never>()
    var data = Data() {
        didSet {
            didChange.send(data)
        }
    }
    
    init(urlString:String) {
        guard let url = URL(string: urlString) else { return }
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else { return }
            DispatchQueue.main.async {
                self.data = data
            }
        }
        task.resume()
    }
}

struct ImageView: View {
    @ObservedObject var imageLoader: ImageLoader
    @State var image = UIImage()
    let itemHeight: CGFloat = 240
    var onTap: ImageClosure?
    
    init(withURL url: String, onTap: ImageClosure? = nil) {
        imageLoader = ImageLoader(urlString: url)
        self.onTap = onTap
    }
    
    var body: some View {
        Image(uiImage: image)
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(width: getWidth(), height: itemHeight)
            .onReceive(imageLoader.didChange) { data in
                self.image = UIImage(data: data) ?? UIImage()
            }
            .onTapGesture {
                onTap?(self.image)
            }
    }
    
    func getWidth() -> CGFloat {
        let width: CGFloat = (UIScreen.main.bounds.size.width - 6) / 2
        return width
    }
}
