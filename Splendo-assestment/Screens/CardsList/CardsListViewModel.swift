//
//  CardsListViewModel.swift
//  Splendo-assestment
//
//  Created by tair on 15.04.22.
//

import Foundation
import Combine
import SwiftUI

struct Filter {
    var type: [TypeEnum] = TypeEnum.allCases
    var rarity: [Rarity] = Rarity.allCases
    var classes: [Class] = Class.allCases
    var mechanics: [Name] = Name.allCases
}

class CardsListViewModel: ObservableObject {
    
    static let fileName = "cards"
    @Published var isLoading = false
    @Published var cardData: CardData
    @Published var cards: [Basic] = []
    @Published var fields: [CardData.CodingKeys] = []
    @Published var currentCategory: CardData.CodingKeys
    private var selectedItem: Basic?
    var filter: Filter = Filter()
    weak var coordinator: MainFlowCoordinatorProtocol?
    
    init(cardData: CardData, coordinator: MainFlowCoordinatorProtocol?) {
        self.coordinator = coordinator
        self.currentCategory = CardData.CodingKeys.basic
        self.cardData = cardData //loadData()
        self.cards = cardData.basic
        self.fields = CardData.CodingKeys.allCases
    }
    
    func set(item: CardData.CodingKeys) {
        self.currentCategory = item
        self.cards = cardData.getCardsBy(item).applied(filter)
    }
    
    func setSelected(card: Basic) {
        selectedItem = card
    }
    
    func applyFilter(filter: Filter) {
        self.filter = filter
        self.cards = cardData.getCardsBy(currentCategory).applied(filter)
    }
    
    func showFilter() -> FilterView? {
        return coordinator?.getFiltersView(with: filter, completion: { filter in
            self.applyFilter(filter: filter)
        })
    }
    
    func showCardView() -> CardView? {
        guard let selectedItem = selectedItem else {
            return nil
        }
        return coordinator?.getCardView(with: selectedItem)
    }
}


