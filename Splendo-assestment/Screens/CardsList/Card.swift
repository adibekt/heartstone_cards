//
//  Card.swift
//  Splendo-assestment
//
//  Created by tair on 17.04.22.
//

import Foundation

// MARK: - CardsResponse
struct CardData: Codable {
    let basic, classic: [Basic]
    let hallOfFame, naxxramas, goblinsVsGnomes, blackrockMountain: [Basic]
    let theGrandTournament, theLeagueOfExplorers, whispersOfTheOldGods, oneNightInKarazhan: [Basic]
    let meanStreetsOfGadgetzan: [Basic]
    let journeyToUnGoro: [Basic]
    let tavernBrawl: [Basic]
    let heroSkins: [Basic]
    let missions: [Basic]
    let credits: [Basic]
    
    enum CodingKeys: String, CodingKey, CaseIterable {
        case basic = "Basic"
        case classic = "Classic"
        case hallOfFame = "Hall of Fame"
        case naxxramas = "Naxxramas"
        case goblinsVsGnomes = "Goblins vs Gnomes"
        case blackrockMountain = "Blackrock Mountain"
        case theGrandTournament = "The Grand Tournament"
        case theLeagueOfExplorers = "The League of Explorers"
        case whispersOfTheOldGods = "Whispers of the Old Gods"
        case oneNightInKarazhan = "One Night in Karazhan"
        case meanStreetsOfGadgetzan = "Mean Streets of Gadgetzan"
        case journeyToUnGoro = "Journey to Un'Goro"
        case tavernBrawl = "Tavern Brawl"
        case heroSkins = "Hero Skins"
        case missions = "Missions"
        case credits = "Credits"
    }
    
    func getCardsBy(_ category: CodingKeys) -> [Basic] {
        switch category {
        case .basic:
            return basic
        case .classic:
            return classic
        case .hallOfFame:
            return hallOfFame
        case .naxxramas:
            return naxxramas
        case .goblinsVsGnomes:
            return goblinsVsGnomes
        case .blackrockMountain:
            return blackrockMountain
        case .theGrandTournament:
            return theGrandTournament
        case .theLeagueOfExplorers:
            return theLeagueOfExplorers
        case .whispersOfTheOldGods:
            return whispersOfTheOldGods
        case .oneNightInKarazhan:
            return oneNightInKarazhan
        case .meanStreetsOfGadgetzan:
            return meanStreetsOfGadgetzan
        case .journeyToUnGoro:
            return journeyToUnGoro
        case .tavernBrawl:
            return tavernBrawl
        case .heroSkins:
            return heroSkins
        case .missions:
            return missions
        case .credits:
            return credits
        }
    }
}

struct Basic: Codable, Identifiable {
    var id: String {
        return cardID
    }
    let cardID, name: String
    let cardSet: BasicCardSet
    let type: TypeEnum
    let text: String?
    let playerClass: Class?
    let locale: Locale
    let mechanics: [Mechanic]?
    let faction: Faction?
    let rarity: Rarity?
    let health: Int?
    let collectible: Bool?
    let img: String?
    let imgGold: String?
    let attack: Int?
    let race: Race?
    let cost: Int?
    let flavor, artist, howToGet, howToGetGold: String?
    let durability: Int?
    let elite: Bool?
    let multiClassGroup: String?
    let classes: [Class]?

    enum CodingKeys: String, CodingKey {
        case cardID = "cardId"
        case name, cardSet, type, text, playerClass, locale, mechanics, faction, rarity, health, collectible, img, imgGold, attack, race, cost, flavor, artist, howToGet, howToGetGold, durability, elite, multiClassGroup, classes
    }
}

enum BasicCardSet: String, Codable {
    case basic = "Basic"
    case blackrockMountain = "Blackrock Mountain"
    case classic = "Classic"
    case goblinsVsGnomes = "Goblins vs Gnomes"
    case hallOfFame = "Hall of Fame"
    case meanStreetsOfGadgetzan = "Mean Streets of Gadgetzan"
    case missions = "Missions"
    case naxxramas = "Naxxramas"
    case oneNightInKarazhan = "One Night in Karazhan"
    case tavernBrawl = "Tavern Brawl"
    case theGrandTournament = "The Grand Tournament"
    case theLeagueOfExplorers = "The League of Explorers"
    case whispersOfTheOldGods = "Whispers of the Old Gods"
    case credits = "Credits"
    case debug = "Debug"
    case heroSkins = "Hero Skins"
    case journeyToUnGoro = "Journey to Un'Goro"
}

enum Class: String, Codable, CaseIterable {
    case deathKnight = "Death Knight"
    case dream = "Dream"
    case druid = "Druid"
    case hunter = "Hunter"
    case mage = "Mage"
    case neutral = "Neutral"
    case paladin = "Paladin"
    case priest = "Priest"
    case rogue = "Rogue"
    case shaman = "Shaman"
    case warlock = "Warlock"
    case warrior = "Warrior"
}

enum Faction: String, Codable {
    case alliance = "Alliance"
    case horde = "Horde"
    case neutral = "Neutral"
}

enum Locale: String, Codable {
    case enUS = "enUS"
}

// MARK: - Mechanic
struct Mechanic: Codable {
    let name: Name
}

enum Name: String, Codable, CaseIterable {
    case adapt = "Adapt"
    case adjacentBuff = "AdjacentBuff"
    case affectedBySpellPower = "AffectedBySpellPower"
    case aiMustPlay = "AIMustPlay"
    case aura = "Aura"
    case battlecry = "Battlecry"
    case charge = "Charge"
    case combo = "Combo"
    case deathrattle = "Deathrattle"
    case discover = "Discover"
    case divineShield = "Divine Shield"
    case enrage = "Enrage"
    case freeze = "Freeze"
    case immuneToSpellpower = "ImmuneToSpellpower"
    case inspire = "Inspire"
    case invisibleDeathrattle = "InvisibleDeathrattle"
    case jadeGolem = "Jade Golem"
    case morph = "Morph"
    case oneTurnEffect = "OneTurnEffect"
    case overload = "Overload"
    case poisonous = "Poisonous"
    case quest = "Quest"
    case secret = "Secret"
    case silence = "Silence"
    case spellDamage = "Spell Damage"
    case stealth = "Stealth"
    case summoned = "Summoned"
    case taunt = "Taunt"
    case windfury = "Windfury"
}

enum Race: String, Codable {
    case beast = "Beast"
    case demon = "Demon"
    case dragon = "Dragon"
    case elemental = "Elemental"
    case mech = "Mech"
    case murloc = "Murloc"
    case orc = "Orc"
    case pirate = "Pirate"
    case totem = "Totem"
}

enum Rarity: String, Codable, CaseIterable {
    case common = "Common"
    case epic = "Epic"
    case free = "Free"
    case legendary = "Legendary"
    case rare = "Rare"
}

enum TypeEnum: String, Codable, CaseIterable {
    case enchantment = "Enchantment"
    case hero = "Hero"
    case heroPower = "Hero Power"
    case minion = "Minion"
    case spell = "Spell"
    case weapon = "Weapon"
}

extension Array where Element == Basic {
    func applied(_ filter: Filter) -> [Element] {
        self.filter { i in
            if let rarity = i.rarity {
                return filter.rarity.contains(rarity)
            } else {
                return true
            }
        }.filter { i in
            return filter.type.contains(i.type)
        }.filter { i in
            if let classification = i.playerClass {
                return filter.classes.contains(classification)
            } else {
                return true
            }
        }.filter { i in
            if let mechanics = i.mechanics {
                let names = mechanics.map({ $0.name })
                var hasOneOfName = false
                filter.mechanics.forEach { name in
                    if names.contains(name) {
                        hasOneOfName = true
                    }
                }
                return hasOneOfName
            } else {
                return true
            }
        }
    }
}
