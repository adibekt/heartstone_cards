//
//  MockCards.swift
//  Splendo-assestment
//
//  Created by tair on 1.05.22.
//

import Foundation

// disable lint
struct MockCardData: CardsLoadingService {
    
    func loadData() -> CardData? {
        let basic = [
            Basic(cardID: "1", name: "basic1", cardSet: .basic, type: .heroPower, text: "", playerClass: .dream, locale: .enUS, mechanics: nil, faction: .alliance, rarity: .epic, health: 1, collectible: false, img: "", imgGold: "", attack: 2, race: .demon, cost: 1, flavor: "", artist: "", howToGet: "", howToGetGold: "", durability: 2, elite: false, multiClassGroup: "", classes: []),
            Basic(cardID: "4", name: "basic2", cardSet: .basic, type: .minion, text: "", playerClass: .dream, locale: .enUS, mechanics: nil, faction: .alliance, rarity: .epic, health: 1, collectible: false, img: "", imgGold: "", attack: 2, race: .demon, cost: 1, flavor: "", artist: "", howToGet: "", howToGetGold: "", durability: 2, elite: false, multiClassGroup: "", classes: []),
            Basic(cardID: "5", name: "basic3", cardSet: .basic, type: .minion, text: "", playerClass: .druid, locale: .enUS, mechanics: nil, faction: .neutral, rarity: .free, health: 1, collectible: false, img: "", imgGold: "", attack: 2, race: .demon, cost: 1, flavor: "", artist: "", howToGet: "", howToGetGold: "", durability: 2, elite: false, multiClassGroup: "", classes: []),
            Basic(cardID: "6", name: "basic4", cardSet: .basic, type: .hero, text: "", playerClass: .deathKnight, locale: .enUS, mechanics: nil, faction: .horde, rarity: .legendary, health: 1, collectible: false, img: "", imgGold: "", attack: 2, race: .demon, cost: 1, flavor: "", artist: "", howToGet: "", howToGetGold: "", durability: 2, elite: false, multiClassGroup: "", classes: [])
        ]
        let classic = [Basic(cardID: "2", name: "classic1", cardSet: .classic, type: .heroPower, text: "", playerClass: .deathKnight, locale: .enUS, mechanics: nil, faction: .horde, rarity: .epic, health: 1, collectible: false, img: "", imgGold: "", attack: 2, race: .demon, cost: 1, flavor: "", artist: "", howToGet: "", howToGetGold: "", durability: 2, elite: false, multiClassGroup: "", classes: [])]
        let hall = [Basic(cardID: "3", name: "hall3", cardSet: .hallOfFame, type: .heroPower, text: "", playerClass: .deathKnight, locale: .enUS, mechanics: nil, faction: .horde, rarity: .epic, health: 1, collectible: false, img: "", imgGold: "", attack: 2, race: .demon, cost: 1, flavor: "", artist: "", howToGet: "", howToGetGold: "", durability: 2, elite: false, multiClassGroup: "", classes: [])]
        let data: CardData = CardData(basic: basic, classic: classic, hallOfFame: hall, naxxramas: [], goblinsVsGnomes: [], blackrockMountain: [], theGrandTournament: [], theLeagueOfExplorers: [], whispersOfTheOldGods: [], oneNightInKarazhan: [], meanStreetsOfGadgetzan: [], journeyToUnGoro: [], tavernBrawl: [], heroSkins: [], missions: [], credits: [])
        return data
    }
}
