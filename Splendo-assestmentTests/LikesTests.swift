//
//  LikesTests.swift
//  Splendo-assestmentTests
//
//  Created by tair on 1.05.22.
//

import XCTest

class LikesTests: XCTestCase {

    func testCardViewModel() {
        guard let item = MockCardData().loadData()?.basic.first else {
            return
        }
        let viewModel = CardViewModel(card: item, coordinator: nil)
        AppProperties.liked = []
        viewModel.onLikePressed()
        XCTAssertTrue(viewModel.isLiked)
        XCTAssertTrue(AppProperties.liked.contains(item.id))
        viewModel.onLikePressed()
        XCTAssertFalse(viewModel.isLiked)
        XCTAssertFalse(AppProperties.liked.contains(item.id))
    }

}
