//
//  Splendo_assestmentTests.swift
//  Splendo-assestmentTests
//
//  Created by tair on 15.04.22.
//

import XCTest
@testable import Splendo_assestment

class FilterTests: XCTestCase {

    func testCardsListViewModel() {
        guard let data = MockCardData().loadData() else {
            return
        }
        let viewModel = CardsListViewModel(cardData: data, coordinator: nil)

        // test filters
        viewModel.applyFilter(filter: Filter(type: [.heroPower], rarity: Rarity.allCases, classes: Class.allCases, mechanics: Name.allCases))
        XCTAssertTrue(viewModel.cards.count == 1)
        viewModel.applyFilter(filter: Filter(type: [.heroPower, .enchantment], rarity: Rarity.allCases, classes: Class.allCases, mechanics: Name.allCases))
        XCTAssertTrue(viewModel.cards.count == 1)
        viewModel.applyFilter(filter: Filter(type: [.heroPower, .enchantment, .minion], rarity: Rarity.allCases, classes: Class.allCases, mechanics: Name.allCases))
        XCTAssertTrue(viewModel.cards.count == 3)
        XCTAssertFalse(viewModel.cards.filter({ $0.type != .heroPower && $0.type != .minion }).count > 0)
        // test switching categories
        viewModel.set(item: .classic)
        XCTAssertTrue(viewModel.cards.count == 1)
        viewModel.set(item: .basic)
        XCTAssertTrue(viewModel.cards.count == 3)
        
    }

}
