//
//  Splendo_assestmentUITests.swift
//  Splendo-assestmentUITests
//
//  Created by tair on 15.04.22.
//

import XCTest

class Splendo_assestmentUITests: XCTestCase {
    let app = XCUIApplication()
    
    func testCardsListView() throws {
        let images = app.images.element
        XCTAssert(images.exists)
    }
    
    func testFilterAppearance() throws {
        app.buttons["filter"].tap()
        let rarityLabel = app.staticTexts["Rarity"]
        XCTAssert(rarityLabel.waitForExistence(timeout: 0.5))
    }
 
    override func setUpWithError() throws {
        continueAfterFailure = false
        app.launch()
    }
}
